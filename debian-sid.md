### ![Debian](icons/debian.png) Debian Sid/Unstable based support

Distros this page applies for: [Siduction](https://siduction.org/)

Users of Debian Stable/Testing could also switch to Sid on their already existing installation. But we won't provide the steps involved here due to them involving some commands that if not done right could result in breaking the system.

However there are [tutorials](https://www.digitalocean.com/community/tutorials/upgrading-debian-to-unstable).

<br><br>

#### Have your system up to date.

```sh
sudo apt upgrade
```

<br><br>


#### MPR for Dwarfs package
```sh
export MAKEDEB_RELEASE='makedeb'

bash -c "$(wget -qO - 'https://shlink.makedeb.org/install')"

sudo apt update

sudo apt install git

git clone https://mpr.hunterwittenborn.com/una-bin.git

cd una-bin && makedeb -si

git clone https://mpr.makedeb.org/dwarfs-bin.git && 

cd dwarfs-bin && makedeb -si
```

<br><br>

#### WineHQ repo

```sh
sudo dpkg --add-architecture i386

sudo mkdir -pm755 /etc/apt/keyrings

sudo wget -O /etc/apt/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key

sudo wget -NP /etc/apt/sources.list.d/ "https://dl.winehq.org/wine-builds/debian/dists/trixie/winehq-trixie.sources"

sudo apt update
```

<br><br>

#### Alternative to MPR if it's unavailable.

Download the [dwarfs deb package](https://gitlab.com/jc141x/setup/-/raw/main/dwarfs-bin_0.9.9-1_amd64.deb) and install it on your system statically.

```sh
sudo apt install ~/Downloads/dwarfs-bin_0.9.9-1_amd64.deb
```


<br><br>

#### Install the required packages.

<br>

```sh
# core packages
sudo dpkg --add-architecture i386

sudo apt update

sudo apt install fuse-overlayfs winehq-staging bubblewrap {libpulse0,gstreamer1.0-plugins-{good,base,ugly}}{:i386,} alsa-utils
```

<br><br>


##### AMD and INTEL APU/GPUs only


```sh
sudo apt install libvulkan1{:i386,} vulkan-tools
```


<br>


##### NVIDIA GPUs only


```sh
sudo apt install nvidia-driver nvidia-settings nvidia-smi nvidia-opencl-icd nvidia-opencl-common libvulkan1{:i386,}
```

<br><br>

##### Install Gamescope (**OPTIONAL**)

Prevents games from locking in the user focus, provides many more features. 

Debian does not provide a package for gamescope. Can be built manually from [Github](https://github.com/ValveSoftware/gamescope).

Likely will prevent proper mouse movement on a lot of games. Workaround:

In ~/.jc141rc

```
# additional flags (run "gamescope --help" for options)
ADDITIONAL_FLAGS="--force-grab-cursor"
```

<br>


#### Run the games
Open up a terminal and then run the following command. Please edit where appropriate.



- Using sh instead of bash does not work. Only use bash or ./ with x permission.


```
bash /Path/to/Game/start.{n/e-w/n-w}.sh
```


<br>


**Or you could add them to your game launcher**. Such as [Heroic Games Launcher](https://heroicgameslauncher.com/).


You just need to treat the releases as 'native/linux' so that the launcher asks you to point to a 'linux executable'.


Which the start script is, if you checked the executable permission in properties.

<br>

#### Post Setup
Now that you followed the setup guide for your distro, check out the [**Post-Setup Page**](post-setup.md) for more info on configuration.
