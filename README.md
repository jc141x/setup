### jc141 Setup Guide

While we try our best to support other distros, best results will always be given on Arch based systems. 

If you don't care in particular what kind of distro you use then we recommend switching to EndeavourOS. 

We can also help people that try it with what knowledge we have as daily users.

#### [![Arch](icons/arch.png) Arch-based setup](arch.md)



Including: [ ☆ **EndeavourOS ☆ - Recommended**](https://endeavouros.com/), [Artix](https://artixlinux.org/), [CachyOS](https://cachyos.org/)



#### [![Debian Sid/Unstable](icons/debian.png) Debian Sid/Unstable-based setup](debian-sid.md)



Including: [Siduction](https://siduction.org/)

#### [![Fedora](icons/fedora.png) Fedora-based setup](fedora.md)



Including: [Ultramarine](https://ultramarine-linux.org/).