### ![Arch](icons/arch.png) Arch based


Including: Endeavour OS, Artix, CachyOS.

Copy and paste the following commands into your terminal.

<br>

1. Add the rumpowered repository.

- If you particularly care about not adding third party repos, you could install **dwarfs** from another source like AUR etc. Doesn't matter.


```sh
echo '
[rumpowered]
Server = https://jc141x.github.io/rumpowered-packages/$arch ' | sudo tee -a /etc/pacman.conf
```
2. Add the multilib repo and sign keys for rumpowered.


```sh
sudo sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
sudo pacman-key --recv-keys cc7a2968b28a04b3
sudo pacman-key --lsign-key cc7a2968b28a04b3
```


3. Force refresh all packages (even if in-date) and update.


```sh
sudo pacman -Syyu
```


4. Install the required packages. (remove dwarfs from the command if you skipped step one)


```sh
# core packages
sudo pacman -S --needed dwarfs fuse-overlayfs bubblewrap wine-staging {lib32-,}{alsa-plugins,libpulse,pipewire,gst-plugins-{good,base,base-libs}} gst-plugins-bad gst-plugins-bad-libs gst-plugins-ugly
```
<br>


##### AMD APU/GPUs only


```sh
sudo pacman -S --needed {lib32-,}{vulkan-radeon,vulkan-icd-loader}
```
- [In accordance with the Arch Wiki.](https://wiki.archlinux.org/title/Vulkan#Installation)


For AMD GPUs please ensure that you do not have installed improper drivers with `sudo pacman -R amdvlk vulkan-amdgpu-pro`. This software breaks the proper driver.


<br>


##### INTEL APU/GPUs only


```sh
sudo pacman -S --needed {lib32-,}{vulkan-intel,vulkan-icd-loader}
```
- [In accordance with the Arch Wiki.](https://wiki.archlinux.org/title/Vulkan#Installation)

<br>


##### NVIDIA GPUs only

**The proprietary NVIDIA driver**

Turing (NV160/TUXXX) and newer
```sh
sudo pacman -S --needed {lib32-,}{libglvnd,nvidia-utils,vulkan-icd-loader} nvidia-open
```

For Maxwell (NV110/GMXXX) through Ada Lovelace (NV190/ADXXX)
```sh
sudo pacman -S --needed {lib32-,}{libglvnd,nvidia-utils,vulkan-icd-loader} nvidia
```

- [In accordance with the Arch Wiki.](https://wiki.archlinux.org/title/Vulkan#Installation)

- For older GPUs see the wiki.

<br><br>


**Alternative: The open source NVIDIA NVK driver**

- Currently behind proprietary driver in performance.

- See [Feature Matrix](https://nouveau.freedesktop.org/FeatureMatrix.html) for what is supported for your GPU.

Remove the proprietary driver first, if installed.

```
sudo pacman -R nvidia nvidia-open nvidia-settings nvidia-utils lib32-nvidia-utils egl-wayland
```

Install the driver.

```
sudo pacman -S --needed {lib32-,}{mesa,vulkan-nouveau}
```

- [In accordance with the Arch Wiki.](https://wiki.archlinux.org/title/Vulkan#Installation)

- If you used a GPU manager, you should also check that the driver is not blacklisted in /etc/modprobe.d.

<br>

##### Install Gamescope (**OPTIONAL**)

Prevents games from locking in the user focus, provides many more features. 

Nvidia proprietary driver needs [additional configuration to work](https://wiki.archlinux.org/title/NVIDIA#DRM_kernel_mode_setting).

```sh
sudo pacman -S --needed gamescope
```

Likely will prevent proper mouse movement on a lot of games. Workaround:

In ~/.jc141rc

```
# additional flags (run "gamescope --help" for options)
ADDITIONAL_FLAGS="--force-grab-cursor"
```

<br>


#### Run the games
Open up a terminal and then run the following command. Please edit where appropriate.



- Using sh instead of bash does not work. Only use bash or ./ with x permission.


```
bash /Path/to/Game/start.{n/e-w/n-w}.sh
```


### ! Major issue !

Since the release of wine 10.2, wine received a major change without warning. This change breaks the compatibility of releases using dxvk aka ones having a start.e-w.sh start script.


If running a game results in receiving an error looking like this:
```sh
0024:err:module:import_dll Library d3d9.dll (which is needed by L"Z:Call.of.Duty.4.Modern.Warfare\\iw3sp.exe") not found
```

Then you need to replace the vulkan.tar.xz archive in files/ with [this one](https://gitlab.com/jc141x/scripting/-/blob/main/vulkan.tar.xz?ref_type=heads)

<br><br><br>

<br>

#### Post Setup

**You could add the games to your game launcher**. Such as [Heroic Games Launcher](https://heroicgameslauncher.com/).


You just need to treat the releases as 'native/linux' so that the launcher asks you to point to a 'linux executable'.


Which the start script is, if you checked the executable permission in properties.

Now that you followed the setup guide for your distro, check out the [**Post-Setup Page**](post-setup.md) for more info on configuration.