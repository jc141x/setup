### ![Fedora](icons/fedora.png) Fedora based support


Including Nobara and Ultramarine.


1. Enable the jc141 copr


```sh
sudo dnf copr enable jc141/DwarFS
```


2. Install the required packages.


```sh
# core packages
sudo dnf install dwarfs wine fuse-overlayfs bubblewrap alsa-lib pulseaudio-libs pipewire gstreamer1-plugins-{good,base}
```


<br>

##### AMD/Intel APU/GPUs only


```sh
sudo dnf install {mesa-vulkan-drivers,vulkan-loader}{.i686,.x86_64}
```


<br>


##### NVIDIA GPUs only


```sh
sudo dnf install https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```


```sh
sudo dnf config-manager --enable fedora-cisco-openh264
```


```sh
sudo dnf install akmod-nvidia {vulkan-loader,libglvnd}{.i686,.x86_64}
```

<br>

##### Install Gamescope (**OPTIONAL**)

Prevents games from locking in the user focus, provides many more features. 

Nvidia proprietary driver needs [additional configuration to work](https://wiki.archlinux.org/title/NVIDIA#DRM_kernel_mode_setting) or use [Nouveau/NVK](https://wiki.archlinux.org/title/Nouveau) driver instead (might be disappointed for now if you play hungry games).

```sh
sudo dnf install gamescope
```

Likely will prevent proper mouse movement on a lot of games. Workaround:

In ~/.jc141rc

```
# additional flags (run "gamescope --help" for options)
ADDITIONAL_FLAGS="--force-grab-cursor"
```

<br>


#### Run the games
Open up a terminal and then run the following command. Please edit where appropriate.



- Using sh instead of bash does not work. Only use bash or ./ with x permission.


```
bash /Path/to/Game/start.{n/e-w/n-w}.sh
```


<br>


**Or you could add them to your game launcher**. Such as [Heroic Games Launcher](https://heroicgameslauncher.com/).


You just need to treat the releases as 'native/linux' so that the launcher asks you to point to a 'linux executable'.


Which the start script is, if you checked the executable permission in properties.

<br>

#### Post Setup
Now that you followed the setup guide for your distro, check out the [**Post-Setup Page**](post-setup.md) for more info on configuration.